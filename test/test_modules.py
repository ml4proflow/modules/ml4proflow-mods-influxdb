import unittest
from ml4proflow import modules
from ml4proflow_mods.influxdb.modules import influxdbSourceModule

class TestModulesModule(unittest.TestCase):
    def setUp(self):
        self.dfm = modules.DataFlowManager()
    
    def test_create_influxdbSourceModule(self):
        dut = influxdbSourceModule(self.dfm, {})
        self.assertIsInstance(dut, influxdbSourceModule)

    # define several tests


if __name__ == '__main__':
    unittest.main()
