from __future__ import annotations  # For class references before definition
from typing import Any, Union
from ml4proflow.modules import DataFlowManager, Module, SourceModule, ExecutableModule # Should be updated, if other classes are used

import pandas as pd
from influxdb_client import InfluxDBClient
from datetime import datetime, timedelta


class influxdbSourceModule(SourceModule, ExecutableModule):
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        SourceModule.__init__(self, dfm, config)
        ExecutableModule.__init__(self, dfm, config)
        
        influxdb_url = self.config.setdefault('influxdb_url', "http://localhost")
        influxdb_port = self.config.setdefault('influxdb_port', 15086)
        influxdb_token = self.config.setdefault('influxdb_token', "u9NIcE2cYxxdaIUUHW-5tegmGtEqfHO758MNOjI8H88f_RMBh6uVooPTeeK6zrZq6Nee6pYKPEOPb49CiKQfIg==")
        self.influxdb_org = self.config.setdefault('influxdb_org', "iap")
        
        self.query_bucket = self.config.setdefault('query_bucket', 'amiro')
        self.query_timestart = self.config.setdefault('query_timestart', '-1y')
        self.query_measurement_id = self.config.setdefault('query_measurementID', '2024-04-08 09:15:18.748321')
        self.query_filter_amiro = self.config.setdefault('query_filter_amiro', 60)
        self.query_filter_tags = self.config.setdefault('query_filter_tags', {'TOPIC': ['TOPICID_ACCELEROMETER', 'TOPICID_GYROSCOPE']})
        self.query_filter = f'  |> filter(fn: (r) => r["_measurement"] == "{self.query_measurement_id}")\n'
        self.query_filter += f'  |> filter(fn: (r) =>r["AMiRo"] == "{self.query_filter_amiro}")\n'   
        
        self.client = InfluxDBClient(url=f'{influxdb_url}:{influxdb_port}', token = influxdb_token, org=self.influxdb_org)
        
    def execute_once(self) -> None:
       
        for tag, value in self.query_filter_tags.items():
            if len(value) > 0: 
                topic_filters = ' or '.join([f'r["{tag}"] == "{sub_value}"' for sub_value in value])
                print(topic_filters)
                self.query_filter += f'  |> filter(fn: (r) => {topic_filters})\n'
            #self.query_filter += f'  |> filter(fn: (r) => r["{tag}"] == "{value}")\n'
            
        query = f'''
        from(bucket: "{self.query_bucket}")
          |> range(start: {self.query_timestart})
          {self.query_filter}
          |> pivot(rowKey:["_time"], columnKey: ["_field"], valueColumn: "_value")
        '''
        
        print(query)
        df = self.client.query_api().query_data_frame(org=self.influxdb_org, query=query)
        if isinstance(df, list): 
            # More than one dataframe
            df = pd.concat(df, ignore_index=True, sort= False)
            
        df = df.sort_values(by=['amiro_ts'])
        #df = df.select_dtypes(include=['number'])
        #df.dropna()
        print(df)
        
        for channel in self.config["channels_push"]:
            self._push_data(channel, df)
            
    def generate_filter_line(self, tag, value):
        if isinstance(value, list):
            conditions = " or ".join([f'r["{tag}"] == "{v}"' for v in value])
        else:
            conditions = f'r["{tag}"] == "{value}"'
        return f'  |> filter(fn: (r) => {conditions})\n'


if __name__ == '__main__':
    dfm = DataFlowManager()
    conn = influxdbSourceModule(dfm, {})
    conn.execute_once()

        